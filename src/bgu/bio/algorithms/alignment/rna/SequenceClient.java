package bgu.bio.algorithms.alignment.rna;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import bgu.bio.io.file.json.JSONException;
import bgu.bio.io.file.json.JSONObject;

/**
 * A class for connecting to the server and receiving the next pair to be
 * compared by a worker
 * 
 * @author milon
 * 
 */
public class SequenceClient {

	private PrintWriter out = null;
	private BufferedReader networkIn = null;
	private Socket theSocket;
	private boolean closed;
	private String serverName;
	private int port;

	/**
	 * Initiate a new Sequence Client
	 * 
	 * @param serverName
	 *            server name / ip
	 * @param port
	 *            port number
	 */
	public SequenceClient(String serverName, int port) {
		this.port = port;
		this.serverName = serverName;
		this.open(serverName, port);
	}

	/**
	 * re-open the connection in case it got close
	 * 
	 * @param serverName
	 *            server name / ip
	 * @param port
	 *            port number
	 * @return true iff the reconnect was successful
	 */
	public boolean open(String serverName, int port) {
		try {
			theSocket = new Socket(serverName, port);
			networkIn = new BufferedReader(new InputStreamReader(
					theSocket.getInputStream()));
			out = new PrintWriter(theSocket.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		this.closed = false;
		return true;
	}

	/**
	 * ask the server for the next pair
	 * 
	 * @param attempt
	 *            number of attempts to try
	 * @return the response in JSON format
	 */
	public JSONObject poll(int attempt) {
		if (this.theSocket.isClosed()) {
			this.open(serverName, port);
		}

		// request
		out.println("{\"command\":\"next\"}");
		out.flush();
		try {
			JSONObject obj = new JSONObject(networkIn.readLine());
			String message = obj.getString("message");
			if (message.equals("TERM")) {
				System.out.println(Thread.currentThread().getName()
						+ " got null");
				this.close();
				return null;
			} else if (message.equals("OK")) {
				return obj;
			}
		} catch (IOException e) {
			// if any attempts left then try again
			if (attempt > 0)
				return this.poll(attempt - 1);
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * sending the results back to the server
	 * 
	 * @param obj
	 *            the results in JSON format
	 * @return true if the update was successful
	 */
	public boolean updateResult(JSONObject obj) {
		if (this.theSocket.isClosed()) {
			this.open(serverName, port);
		}
		try {
			obj.put("command", "update");
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		out.println(obj);
		out.flush();

		try {
			JSONObject ans = new JSONObject(networkIn.readLine());
			return ans.getString("message").equals("OK");

		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * see {@link #poll(int)}
	 * 
	 * @return
	 * @throws JSONException
	 */
	public JSONObject poll() throws JSONException {
		if (this.closed)
			return null;

		return this.poll(3);
	}

	/**
	 * close the connection
	 * 
	 * @return true if there where no errors while closing
	 */
	public boolean close() {
		this.closed = true;
		try {
			if (networkIn != null)
				networkIn.close();
			if (out != null)
				out.close();
			return true;
		} catch (IOException e) {
			return false;
		}

	}

	/**
	 * update the total time to the server
	 * 
	 * @param totalTime
	 *            current total working time
	 * @return true if update was successful
	 * @throws JSONException
	 */
	public boolean updateTime(long totalTime) throws JSONException {
		if (this.theSocket.isClosed()) {
			this.open(serverName, port);
		}
		JSONObject obj = new JSONObject();
		obj.put("command", "report_time");
		obj.put("time", totalTime);
		out.println(obj);
		out.flush();

		try {
			JSONObject ans = new JSONObject(networkIn.readLine());
			return ans.getString("message").equals("OK");

		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

}
