package bgu.bio.algorithms.alignment.rna.worker;

import java.util.Arrays;
import java.util.Properties;
import java.util.Random;

import org.apache.log4j.Logger;

import bgu.bio.adt.rna.RNASpecificTree;
import bgu.bio.adt.rna.costs.RnaSpecificCostFunction;
import bgu.bio.adt.rna.costs.RnaSpecificTypeRelatedTreePruning;
import bgu.bio.algorithms.alignment.rna.EngineType;
import bgu.bio.algorithms.alignment.rna.server.ComparisonType;
import bgu.bio.algorithms.graphs.hsa.HSA;
import bgu.bio.algorithms.graphs.hsa.matchers.OrderedMatcherFactory;
import bgu.bio.algorithms.graphs.hsa.matchers.UnorderedMatcherFactory;
import bgu.bio.io.file.json.JSONException;
import bgu.bio.io.file.json.JSONObject;

public class EVDWorker extends RNATreeHSAWorker {

	private int numOfValues;
	private Random rand;
	private int currentCounter;
	private double[] optScores;
	private static final double INIT = Double.NEGATIVE_INFINITY;

	public EVDWorker(int numOfValues) {
		rand = new Random();
		this.numOfValues = numOfValues;
	}

	@Override
	public void init(Properties prop, Logger log) {
		super.init(prop, log);
		this.optScores = new double[scores.length];
		Arrays.fill(this.optScores, INIT);
	}

	@Override
	protected void buildEngines(ComparisonType type, int maxSize,
			RnaSpecificCostFunction cost, boolean local) {

		if (type.equals(ComparisonType.EVD)) {
			engines = new HSA[4];
			engineTypes = new EngineType[4];
			pruning = new RnaSpecificTypeRelatedTreePruning[4];
			HSA hsaOrdered = new HSA(maxSize, maxSize, cost,
					new OrderedMatcherFactory());
			HSA hsaUnordered = new HSA(maxSize, maxSize, cost,
					new UnorderedMatcherFactory());

			RnaSpecificTypeRelatedTreePruning pruneRooted = new RnaSpecificTypeRelatedTreePruning(
					local, true);
			RnaSpecificTypeRelatedTreePruning pruneUnRooted = new RnaSpecificTypeRelatedTreePruning(
					local, false);

			// RO
			engines[0] = hsaOrdered;
			pruning[0] = pruneRooted;
			engineTypes[0] = EngineType.RO;
			// RU
			engines[1] = hsaUnordered;
			pruning[1] = pruneRooted;
			engineTypes[1] = EngineType.RU;
			// UO
			engines[2] = hsaOrdered;
			pruning[2] = pruneUnRooted;
			engineTypes[2] = EngineType.UO;
			// UU
			engines[3] = hsaUnordered;
			pruning[3] = pruneUnRooted;
			engineTypes[3] = EngineType.UU;
		} else {
			throw new IllegalArgumentException(ComparisonType.class.getName()
					+ " must be set to " + ComparisonType.EVD.toString());
		}
	}

	@Override
	protected void updateTask(JSONObject task) throws JSONException {
		for (int test = 0; test < optScores.length; test++) {
			task.put("score" + (test + 1),
					Double.doubleToLongBits(optScores[test]));
		}
		task.put("scores", optScores.length);
		task.put("pval", 0);
	}

	@Override
	protected boolean compare(JSONObject task) throws JSONException {
		int i = task.getInt("ind1");
		int j = task.getInt("ind2");

		if (i < 0 || j < 0) {
			stop();
			return false;
		}

		final RNASpecificTree t = trees.get(i);
		final RNASpecificTree s = trees.get(j);

		t.shuffle(rand);
		s.shuffle(rand);

		if (currentCounter == numOfValues) {
			// clean left overs from the last time
			Arrays.fill(this.optScores, Double.NEGATIVE_INFINITY);
			currentCounter = 0;
		}
		currentCounter++;

		compute(t, s);
		RnaSpecificTypeRelatedTreePruning prune = pruning[amountOfTests - 1];
		HSA hsaEngine = engines[amountOfTests - 1];

		// compute the relative score
		prune.calculateCost(t);
		double selfT = hsaEngine.computeHSA(t, t);
		prune.calculateCost(s);
		double selfS = hsaEngine.computeHSA(s, s);

		double avg = 2.0 / (selfT + selfS);
		for (int test = 0; test < optScores.length; test++) {
			scores[test] = scores[test] * avg;
		}

		// copy the minimums to the minScore array
		for (int test = 0; test < optScores.length; test++) {
			if (optScores[test] < scores[test]) {
				optScores[test] = scores[test];
			}
		}

		if (currentCounter == numOfValues) {
			// init the maximum
			logger.info("Committed minimum to server "
					+ optScores[optScores.length - 1]);
			return true;
		}

		return false;
	}

	@Override
	protected void afterCompare(JSONObject task) throws JSONException {
	}

}
