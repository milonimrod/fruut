package bgu.bio.algorithms.alignment.rna.worker;

import bgu.bio.adt.rna.RNASpecificTree;
import bgu.bio.adt.rna.costs.RnaSpecificCostFunction;
import bgu.bio.adt.rna.costs.RnaSpecificTypeRelatedTreePruning;
import bgu.bio.algorithms.alignment.rna.EngineType;
import bgu.bio.algorithms.alignment.rna.server.ComparisonType;
import bgu.bio.algorithms.graphs.hsa.HSA;
import bgu.bio.algorithms.graphs.hsa.matchers.OrderedMatcherFactory;
import bgu.bio.algorithms.graphs.hsa.matchers.UnorderedMatcherFactory;
import bgu.bio.io.file.json.JSONException;
import bgu.bio.io.file.json.JSONObject;

public class GeneralWorker extends RNATreeHSAWorker {

	@Override
	protected void buildEngines(ComparisonType type, int maxSize,
			RnaSpecificCostFunction cost, boolean useCavity) {

		if (type.equals(ComparisonType.ROOTING)
				|| type.equals(ComparisonType.UNORDERED_ROOTING)) {
			engines = new HSA[2];
			engineTypes = new EngineType[2];
			pruning = new RnaSpecificTypeRelatedTreePruning[2];
			engineTypes[0] = type.equals(ComparisonType.ROOTING) ? EngineType.RO
					: EngineType.RU;
			engineTypes[1] = type.equals(ComparisonType.ROOTING) ? EngineType.UO
					: EngineType.UU;
			engines[0] = new HSA(
					maxSize,
					maxSize,
					cost,
					type.equals(ComparisonType.ROOTING) ? new OrderedMatcherFactory()
							: new UnorderedMatcherFactory(), useCavity);
			engines[1] = engines[0];

			pruning[0] = new RnaSpecificTypeRelatedTreePruning(false, true);
			pruning[1] = new RnaSpecificTypeRelatedTreePruning(false, false);
		} else if (type.equals(ComparisonType.ORDERING)
				|| type.equals(ComparisonType.UNROOTED_ORDERING)) {
			engines = new HSA[2];
			engineTypes = new EngineType[2];
			pruning = new RnaSpecificTypeRelatedTreePruning[2];
			engineTypes[0] = type.equals(ComparisonType.ORDERING) ? EngineType.RO
					: EngineType.UO;
			engineTypes[1] = type.equals(ComparisonType.ORDERING) ? EngineType.RU
					: EngineType.UU;
			engines[0] = new HSA(maxSize, maxSize, cost,
					new OrderedMatcherFactory(), useCavity);
			engines[1] = new HSA(maxSize, maxSize, cost,
					new UnorderedMatcherFactory(), useCavity);

			pruning[0] = new RnaSpecificTypeRelatedTreePruning(false,
					!type.equals(ComparisonType.UNROOTED_ORDERING));
			pruning[1] = pruning[0];
		} else if (type.equals(ComparisonType.UNROOTED_UNORDERED)) {
			engines = new HSA[2];
			engineTypes = new EngineType[2];
			pruning = new RnaSpecificTypeRelatedTreePruning[2];
			engineTypes[0] = EngineType.RO;
			engineTypes[1] = EngineType.UU;
			engines[0] = new HSA(maxSize, maxSize, cost,
					new OrderedMatcherFactory(), useCavity);
			engines[1] = new HSA(maxSize, maxSize, cost,
					new UnorderedMatcherFactory(), useCavity);

			pruning[0] = new RnaSpecificTypeRelatedTreePruning(false, true);
			pruning[1] = new RnaSpecificTypeRelatedTreePruning(false, false);
		} else if (type.equals(ComparisonType.RO_RU_UU)) {
			engines = new HSA[3];
			engineTypes = new EngineType[3];
			pruning = new RnaSpecificTypeRelatedTreePruning[3];

			engineTypes[0] = EngineType.RO;
			engineTypes[1] = EngineType.RU;
			engineTypes[2] = EngineType.UU;

			engines[0] = new HSA(maxSize, maxSize, cost,
					new OrderedMatcherFactory(), useCavity);
			engines[1] = new HSA(maxSize, maxSize, cost,
					new UnorderedMatcherFactory(), useCavity);
			engines[2] = engines[1];

			pruning[0] = new RnaSpecificTypeRelatedTreePruning(false, true);
			pruning[1] = pruning[0];
			pruning[2] = new RnaSpecificTypeRelatedTreePruning(false, false);
		} else if (type.equals(ComparisonType.ALL)) {
			engines = new HSA[4];
			engineTypes = new EngineType[4];
			pruning = new RnaSpecificTypeRelatedTreePruning[4];
			HSA hsaOrdered = new HSA(maxSize, maxSize, cost,
					new OrderedMatcherFactory());
			HSA hsaUnordered = new HSA(maxSize, maxSize, cost,
					new UnorderedMatcherFactory());
			RnaSpecificTypeRelatedTreePruning pruneRooted = new RnaSpecificTypeRelatedTreePruning(
					false, true);
			RnaSpecificTypeRelatedTreePruning pruneUnRooted = new RnaSpecificTypeRelatedTreePruning(
					false, false);

			// RO
			engines[0] = hsaOrdered;
			pruning[0] = pruneRooted;
			engineTypes[0] = EngineType.RO;
			// RU
			engines[1] = hsaUnordered;
			pruning[1] = pruneRooted;
			engineTypes[1] = EngineType.RU;
			// UO
			engines[2] = hsaOrdered;
			pruning[2] = pruneUnRooted;
			engineTypes[2] = EngineType.UO;
			// UU
			engines[3] = hsaUnordered;
			pruning[3] = pruneUnRooted;
			engineTypes[3] = EngineType.UU;
		}
	}

	@Override
	protected void updateTask(JSONObject task) throws JSONException {
		for (int test = 0; test < scores.length; test++) {
			task.put("score" + (test + 1),
					Double.doubleToLongBits(scores[test]));
		}
		task.put("scores", scores.length);
	}

	@Override
	protected boolean compare(JSONObject task) throws JSONException {
		int i = task.getInt("ind1");
		int j = task.getInt("ind2");

		if (i < 0 || j < 0) {
			stop();
			return false;
		}

		final RNASpecificTree t = trees.get(i);
		final RNASpecificTree s = trees.get(j);

		compute(t, s);
		logger.info("computed on sizes " + t.getNodeNum() + " " + s.getNodeNum());

		return true;
	}

	@Override
	protected void afterCompare(JSONObject task) throws JSONException {
	}

}
