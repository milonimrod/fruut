package bgu.bio.algorithms.alignment.rna.worker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.log4j.Logger;

import bgu.bio.adt.rna.RNASpecificTree;
import bgu.bio.adt.rna.costs.RnaSpecificCostFunction;
import bgu.bio.adt.rna.costs.RnaSpecificSmoothCost;
import bgu.bio.adt.rna.costs.RnaSpecificTypeRelatedTreePruning;
import bgu.bio.adt.rna.filter.RNATreeFilter;
import bgu.bio.algorithms.alignment.AffineGapGlobalSequenceAlignmentNoMatrix;
import bgu.bio.algorithms.alignment.CacheMemorySequenceAlignment;
import bgu.bio.algorithms.alignment.SequenceAlignment;
import bgu.bio.algorithms.alignment.rna.EngineType;
import bgu.bio.algorithms.alignment.rna.SequenceClient;
import bgu.bio.algorithms.alignment.rna.server.AllAgaintAllServer;
import bgu.bio.algorithms.alignment.rna.server.ComparisonType;
import bgu.bio.algorithms.graphs.hsa.HSA;
import bgu.bio.io.file.json.JSONException;
import bgu.bio.io.file.json.JSONObject;
import bgu.bio.io.file.properties.PropertiesUtils;
import bgu.bio.util.AffineGapScoringMatrix;
import bgu.bio.util.ScoringMatrix;
import bgu.bio.util.alphabet.RNABasePairAlphabet;
import bgu.bio.util.alphabet.RnaAlphabet;

public abstract class RNATreeHSAWorker {

	protected HSA[] engines;
	protected EngineType[] engineTypes;
	protected RnaSpecificTypeRelatedTreePruning[] pruning;
	protected Logger logger;

	protected SequenceClient queue;
	protected ArrayList<RNASpecificTree> trees;
	protected ArrayList<RNASpecificTree> treesShuffled;

	protected int amountOfTests;
	protected double[] scores;

	protected boolean selfScore;

	private boolean shouldStop;

	public void init(Properties prop, Logger log) {
		this.logger = log;
		RNATreeFilter filter = null;
		try {
			String[] sp = prop.getProperty("filter").split("\\|");
			String name = sp[0];
			String params = sp.length == 2 ? sp[1] : null;
			filter = (RNATreeFilter) PropertiesUtils.initiateFromProps(name,
					params, prop);
		} catch (IOException e) {
			e.printStackTrace();
		}

		String[] input = prop.getProperty("input").split(",");
		trees = new ArrayList<RNASpecificTree>();
		treesShuffled = new ArrayList<RNASpecificTree>();

		AllAgaintAllServer.loadTrees(input, filter, trees);
		AllAgaintAllServer.loadTrees(input, filter, treesShuffled);

		// give prune and smooth costs for both lists
		RnaSpecificSmoothCost smooth = new RnaSpecificSmoothCost();

		int maxSize = 0;
		for (RNASpecificTree tree : trees) {
			if (tree.getNodeNum() > maxSize) {
				maxSize = tree.getNodeNum();
			}
			smooth.calculateCost(tree);
		}

		for (RNASpecificTree tree : treesShuffled) {
			smooth.calculateCost(tree);
		}

		SequenceAlignment aligner = new CacheMemorySequenceAlignment(5,
				new AffineGapGlobalSequenceAlignmentNoMatrix(10, 10,
						RnaAlphabet.getInstance(), new AffineGapScoringMatrix(
								prop.getProperty("scoring.interval"),
								RnaAlphabet.getInstance())));

		RnaSpecificCostFunction cost = new RnaSpecificCostFunction(
				new ScoringMatrix(prop.getProperty("scoring.bp"),
						RNABasePairAlphabet.getInstance()), aligner);

		selfScore = Boolean.parseBoolean(prop.getProperty("self", "false"));
		final boolean local = Boolean.parseBoolean(prop.getProperty(
				"alignment.local", "false"));
		final ComparisonType type = ComparisonType.valueOf(prop
				.getProperty("type"));

		buildEngines(type, maxSize, cost, local);

		amountOfTests = engines.length;

		scores = new double[selfScore ? amountOfTests + 2 : amountOfTests];

		connect(prop);
	}

	private void connect(Properties prop) {
		queue = new SequenceClient(prop.getProperty("server"),
				Integer.parseInt(prop.getProperty("port")));
	}

	protected void close() {
		queue.close();
	}

	protected abstract void buildEngines(ComparisonType type, int maxSize,
			RnaSpecificCostFunction cost, boolean localAlignment);

	protected JSONObject getNextPair() throws JSONException {
		return queue.poll();
	}

	private boolean submitResult(JSONObject task) {
		return queue.updateResult(task);
	}

	public void stop() {
		this.shouldStop = true;
	}

	protected abstract boolean compare(JSONObject task) throws JSONException;

	protected abstract void afterCompare(JSONObject task) throws JSONException;

	protected abstract void updateTask(JSONObject task) throws JSONException;

	protected void compute(RNASpecificTree t, RNASpecificTree s) {
		for (int test = 0; test < amountOfTests; test++) {
			pruning[test].calculateCost(t);
			pruning[test].calculateCost(s);

			scores[test] = engines[test].computeHSA(t, s);
		}

		if (selfScore) {
			RnaSpecificTypeRelatedTreePruning prune = pruning[amountOfTests - 1];
			HSA hsaEngine = engines[amountOfTests - 1];

			prune.calculateCost(t);
			scores[scores.length - 2] = hsaEngine.computeHSA(t, t);
			prune.calculateCost(s);
			scores[scores.length - 1] = hsaEngine.computeHSA(s, s);
		}
	}

	public void run() {
		shouldStop = false;
		try {
			JSONObject task = getNextPair();
			while (!shouldStop && task != null) {
				final boolean ans = compare(task);
				afterCompare(task);
				if (ans) {
					updateTask(task);
					submitResult(task);
				}
				task = getNextPair();
			}
		} catch (JSONException ex) {

		}
		close();
	}
}
