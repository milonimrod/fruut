package bgu.bio.algorithms.alignment.rna.worker;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Properties;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.RealDistribution;
import org.apache.log4j.Logger;

import bgu.bio.algorithms.alignment.rna.EngineType;
import bgu.bio.io.file.json.JSONException;
import bgu.bio.io.file.json.JSONObject;

public class StatisticalWorker extends GeneralWorker {

	private HashMap<EngineType, RealDistribution> map;

	public StatisticalWorker() {
		map = new HashMap<EngineType, RealDistribution>();
	}

	@Override
	public void init(Properties props, Logger log) {
		super.init(props, log);
		selfScore = true;
		this.scores = new double[scores.length + amountOfTests + 2];
		String[] types = props.getProperty("stats.types", "").split(",");
		for (int i = 0; i < types.length; i++) {
			types[i] = types[i].trim();
			double mu = Double.parseDouble(props.getProperty("stats."
					+ types[i] + ".mu", "0"));
			double sigma = Double.parseDouble(props.getProperty("stats."
					+ types[i] + ".sigma", "1"));
			EngineType type = EngineType.valueOf(types[i]);
			map.put(type, new NormalDistribution(mu, sigma));
		}
	}

	@Override
	protected void updateTask(JSONObject task) throws JSONException {
		super.updateTask(task);
	}

	@Override
	protected boolean compare(JSONObject task) throws JSONException {
		boolean ans = super.compare(task);
		if (!ans) {
			// in case the parent returned false
			return false;
		}

		// check if the raw score has not improved between the comparison types
		if (Math.abs(scores[0] - scores[amountOfTests]) < 0.0001) {
			logger.info("Ignores score " + scores[0] + " no improvement");
			return false;
		}

		// calculate the pvalue
		final double avg = (scores[scores.length - 1] + scores[scores.length - 2]) / 2.0;
		for (int i = 0; i < amountOfTests; i++) {
			double rel = scores[i] / avg;
			// calculate the pvalue
			RealDistribution dist = map.get(this.engineTypes[i]);
			scores[amountOfTests + i] = 1 - dist.cumulativeProbability(rel);
		}
		boolean foundPval = false;
		// pass on all the p-values and check that there is at least one that is
		// all smaller then 1*10^-3
		for (int i = 0; i < amountOfTests && !foundPval; i++) {
			foundPval = scores[amountOfTests + i] <= 0.001;
		}
		if (!foundPval) {
			return false;
		}

		logger.info("returned result " + Arrays.toString(scores));
		return true;
	}
}