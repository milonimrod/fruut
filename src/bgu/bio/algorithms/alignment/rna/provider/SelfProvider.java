package bgu.bio.algorithms.alignment.rna.provider;

import gnu.trove.list.array.TIntArrayList;

import java.util.List;
import java.util.Random;

import bgu.bio.adt.rna.RNASpecificTree;
import bgu.bio.adt.tuples.IntPair;

/**
 * A provider that creates each given tree only against itself.
 * @author milon
 *
 */
public class SelfProvider implements PairProvider {
	private TIntArrayList ids;
	private int position;

	@Override
	public boolean hasNext() {
		return position < ids.size();
	}

	@Override
	public IntPair next() {
		final int id = this.ids.getQuick(this.position);
		IntPair ans = new IntPair(id,id);
		this.position++;
		return ans;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void initiate(List<List<RNASpecificTree>> list, boolean shuffle) {
		ids = new TIntArrayList();
		for (List<RNASpecificTree> inner : list){
			for (RNASpecificTree tree : inner){
				ids.add(tree.getId());
			}
		}
		if (shuffle){
			ids.shuffle(new Random());
		}
		this.position = 0;
	}

	@Override
	public void stop() {
		this.position = this.ids.size();
	}

	@Override
	public int initialAmount() {
		return ids.size();
	}
}
