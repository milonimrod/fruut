package bgu.bio.algorithms.alignment.rna.provider;

import gnu.trove.list.array.TIntArrayList;

import java.util.List;
import java.util.Random;

import bgu.bio.adt.rna.RNASpecificTree;
import bgu.bio.adt.tuples.IntPair;

/**
 * This provider is for the calculation of an E-value score for the pairs. Notice that a pair can't contain the same tree 
 * @author milon
 *
 */
public class EvalProvider implements PairProvider {
	/**
	 * tree ids
	 */
	private TIntArrayList ids;
	/**
	 * number of pairs left to return
	 */
	private int num;
	/**
	 * amount of shuffled pairs to return 
	 */
	private final int amount;
	/**
	 * the random class used to shuffle the pair
	 */
	private Random rand;

	/**
	 * Initiate a new EVAL score pair provider
	 * @param amount number of pairs to provide
	 */
	public EvalProvider(int amount) {
		rand = new Random();
		this.amount = amount;
	}
	
	
	/**
	 * Initiate a new EVAL score pair provider
	 * @param amount number of pairs to provide
	 * @param seed the seed to be used in the shuffle process
	 */
	public EvalProvider(int amount,long seed) {
		rand = new Random(seed);
		this.amount = amount;
	}

	@Override
	public boolean hasNext() {
		return num < amount;
	}

	@Override
	public IntPair next() {
		final int id1 = this.ids.getQuick(rand.nextInt(ids.size()));	
		int id2 = id1;
		//make sure the same id is not selected twice
		while (id2 == id1){
			id2=this.ids.getQuick(rand.nextInt(ids.size()));
		}
		
		IntPair ans = new IntPair(id1, id2);
		this.num++;
		return ans;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void initiate(List<List<RNASpecificTree>> list, boolean shuffle) {
		ids = new TIntArrayList();
		for (List<RNASpecificTree> inner : list) {
			for (RNASpecificTree tree : inner) {
				ids.add(tree.getId());
			}
		}
		if (shuffle) {
			ids.shuffle(new Random());
		}
		this.num = 0;
	}

	@Override
	public void stop() {
		this.num = amount;
	}

	@Override
	public int initialAmount() {
		return amount;
	}
}
