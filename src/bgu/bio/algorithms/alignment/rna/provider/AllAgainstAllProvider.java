package bgu.bio.algorithms.alignment.rna.provider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import bgu.bio.adt.rna.RNASpecificTree;
import bgu.bio.adt.tuples.IntPair;

/**
 * This is an all against all provider.
 * @author milon
 *
 */
public class AllAgainstAllProvider implements PairProvider {
	private List<IntPair> ids;

	/* (non-Javadoc)
	 * @see java.util.Iterator#hasNext()
	 */
	@Override
	public boolean hasNext() {
		return ids.size() != 0;
	}

	/* (non-Javadoc)
	 * @see java.util.Iterator#next()
	 */
	@Override
	public IntPair next() {
		IntPair current = ids.remove(ids.size() - 1);
		return current;
	}
	
	/* (non-Javadoc)
	 * @see java.util.Iterator#remove()
	 */
	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.alignment.rna.provider.PairProvider#initiate(java.util.List, boolean)
	 */
	@Override
	public void initiate(List<List<RNASpecificTree>> list, boolean shuffle) {
		ids = new ArrayList<IntPair>();
		ArrayList<RNASpecificTree> temp = new ArrayList<RNASpecificTree>();
		for (List<RNASpecificTree> inner : list) {
			for (RNASpecificTree tree : inner) {
				temp.add(tree);
			}
		}

		final int amountOfTrees = temp.size();
		for (int i = 0; i < amountOfTrees; i++) {
			for (int j = i + 1; j < amountOfTrees; j++) {
				this.ids.add(new IntPair(temp.get(i).getId(), temp.get(j)
						.getId()));
			}
		}
		if (shuffle) {
			Collections.shuffle(ids);
		}
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.alignment.rna.provider.PairProvider#stop()
	 */
	@Override
	public void stop() {
		this.ids.clear();
	}
	
	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.alignment.rna.provider.PairProvider#initialAmount()
	 */
	@Override
	public int initialAmount() {
		return ids.size();
	}
}
