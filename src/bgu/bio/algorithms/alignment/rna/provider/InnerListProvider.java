package bgu.bio.algorithms.alignment.rna.provider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import bgu.bio.adt.rna.RNASpecificTree;
import bgu.bio.adt.tuples.IntPair;

/**
 * A provider that creates for each given family an all against all provider
 * @author milon
 *
 */
public class InnerListProvider implements PairProvider {
	private List<IntPair> ids;

	@Override
	public boolean hasNext() {
		return ids.size() != 0;
	}

	@Override
	public IntPair next() {
		IntPair current = ids.remove(ids.size() - 1);
		return current;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void initiate(List<List<RNASpecificTree>> list, boolean shuffle) {
		ids = new ArrayList<IntPair>();
		for (List<RNASpecificTree> inner : list) {
			final int amountOfTrees = inner.size();
			for (int i = 0; i < amountOfTrees; i++) {
				for (int j = i + 1; j < amountOfTrees; j++) {
					this.ids.add(new IntPair(inner.get(i).getId(), inner.get(j)
							.getId()));
				}
			}
		}

		if (shuffle) {
			Collections.shuffle(ids);
		}
	}

	@Override
	public void stop() {
		this.ids.clear();
	}

	@Override
	public int initialAmount() {
		return ids.size();
	}
}
