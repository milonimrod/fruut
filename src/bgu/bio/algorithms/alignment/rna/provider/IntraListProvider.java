package bgu.bio.algorithms.alignment.rna.provider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import bgu.bio.adt.rna.RNASpecificTree;
import bgu.bio.adt.tuples.IntPair;

/**
 * A provider that creates for each pair of families an all against all provider.
 * @author milon
 *
 */
public class IntraListProvider implements PairProvider {
	private List<IntPair> ids;

	@Override
	public boolean hasNext() {
		return ids.size() != 0;
	}

	@Override
	public IntPair next() {
		IntPair current = ids.remove(ids.size() - 1);
		return current;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void initiate(List<List<RNASpecificTree>> list, boolean shuffle) {
		ids = new ArrayList<IntPair>();
		if (list.size() < 2){
			return;
		}
		for (int l1 = 0; l1 < list.size(); l1++) {
			List<RNASpecificTree> list1 = list.get(l1);			
			for (int l2 = l1+1; l2 < list.size(); l2++) {
				List<RNASpecificTree> list2 = list.get(l2);	
				for (int i = 0; i < list1.size(); i++) {
					for (int j = 0; j < list2.size(); j++) {
						this.ids.add(new IntPair(list1.get(i).getId(), list2.get(j)
								.getId()));
					}
				}
			}
		}

		if (shuffle) {
			Collections.shuffle(ids);
		}
	}

	@Override
	public void stop() {
		this.ids.clear();
	}

	@Override
	public int initialAmount() {
		return ids.size();
	}
}
