package bgu.bio.algorithms.alignment.rna.provider;

import java.util.Iterator;
import java.util.List;

import bgu.bio.adt.rna.RNASpecificTree;
import bgu.bio.adt.tuples.IntPair;

/**
 * This interface represents a provider of pairs to be compared using the FRUUT
 * tool
 * 
 * @author milon
 * 
 */
public interface PairProvider extends Iterator<IntPair> {

	/**
	 * initiates the provider on the given lists of families
	 * 
	 * @param list
	 *            - the list of families, each entry in the list is a family
	 * @param shuffle
	 *            - should the pairs be returned in a random manner
	 */
	public void initiate(List<List<RNASpecificTree>> list, boolean shuffle);

	/**
	 * stop providing pairs. sets {@link #hasNext()} to return false.
	 */
	public void stop();

	/**
	 * returns the initial amount of pairs in the provider
	 * 
	 * @return the initial amount of pairs in the provider
	 */
	public int initialAmount();
}
