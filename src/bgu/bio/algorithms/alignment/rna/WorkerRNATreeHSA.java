package bgu.bio.algorithms.alignment.rna;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import bgu.bio.algorithms.alignment.rna.worker.GeneralWorker;
import bgu.bio.algorithms.alignment.rna.worker.RNATreeHSAWorker;
import bgu.bio.io.file.json.JSONException;
import bgu.bio.io.file.properties.PropertiesUtils;

public class WorkerRNATreeHSA {

	public static void main(String[] args) throws FileNotFoundException,
			IOException, JSONException {
		// load file content
		Properties prop = new Properties();
		prop.load(new FileInputStream(args[0]));
		
		//check worker
		RNATreeHSAWorker worker = null;
		try {
			String[] sp = prop.getProperty("worker",GeneralWorker.class.getCanonicalName()).split("\\|");
			String name = sp[0];
			String params = sp.length == 2 ? sp[1] : null;
			worker = (RNATreeHSAWorker) PropertiesUtils.initiateFromProps(name,
					params, prop);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Logger logger = Logger.getLogger(WorkerRNATreeHSA.class);
		PropertyConfigurator.configure(prop);

		worker.init(prop,logger);
		worker.run();
	}
}
