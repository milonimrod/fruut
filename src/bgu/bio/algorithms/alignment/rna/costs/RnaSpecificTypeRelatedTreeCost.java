package bgu.bio.algorithms.alignment.rna.costs;

import java.util.Arrays;

import bgu.bio.adt.graphs.Tree;
import bgu.bio.adt.queue.Pool;
import bgu.bio.adt.rna.NodeLabel;
import bgu.bio.adt.rna.RNANodeLabel;

/**
 * The Class SizeRelatedTreePruning calculates the cost of pruning a sub-tree
 * according to the size of the sub-tree (i.e. the number of nodes in the
 * sub-tree). This class is not thread safe. it uses inner fields to store data
 * and prevent memoryAllocation
 */
public class RnaSpecificTypeRelatedTreeCost{

	private Pool<int[]> pool;
	int[][][] elementCountersT;
	int[][][] elementCountersS;

	private static int elementNum = 0;
	private static final int UNPAIRED_BASES = elementNum++;
	private static final int BASE_PAIRS = elementNum++;
	private static final int HAIRPINS = elementNum++;
	private static final int MULTI_LOOPS = elementNum++;
	private static final int INTERNAL_LOOPS = elementNum++;
	private static final int INTERVAL_BASES = elementNum++;
	private static final int EXTERNAL_LOOP = elementNum++;
	private static final int ROOT = elementNum++;

	public RnaSpecificTypeRelatedTreeCost() {
		this.pool = new Pool<int[]>();
	}

	private void calculateTreeCounters(Tree tree,int[][][] elementCounters) {
		int[][] edges = tree.getEdges();

		if (elementCounters == null || elementCounters.length < edges.length) {
			elementCounters = new int[edges.length][][];
		}
		for (int i = 0; i < edges.length; i++) {

			if (elementCounters[i] == null
					|| elementCounters[i].length < edges[i].length) {
				elementCounters[i] = new int[edges[i].length][elementNum];
			} else {
				for (int x = 0; x < edges[i].length; x++) {
					int[] ans = pool.dequeue();
					if (ans == null) {
						ans = new int[elementNum];
					}
					elementCounters[i][x] = ans;
				}
			}
		}

		final int num = tree.getEdgeNum();
		for (int i = 0; i < num; i++) {
			int[] edge = tree.edgeAtIndex(i);
			final int from = edge[0];
			final int to = tree.getNeighbor(from, edge[1]);
			final int toIdx = edge[1];

			final int deg = tree.outDeg(to);

			for (int n = 0; n < deg; n++) {
				final int sub = tree.getNeighbor(to, n);
				if (sub != from) {
					for (int c = 0; c < elementNum; c++) {
						elementCounters[from][toIdx][c] += elementCounters[to][n][c];
					}
				}
			}

			// add to's data
			NodeLabel label = tree.getLabel(to);
			switch (label.getType()) {
			case RNANodeLabel.BASE_INTERVAL:
				elementCounters[from][toIdx][UNPAIRED_BASES] += label.getLabelValue().size();
				elementCounters[from][toIdx][INTERVAL_BASES]++;
				break;
			case RNANodeLabel.BASE_PAIR:
				elementCounters[from][toIdx][BASE_PAIRS]++;
				break;
			case RNANodeLabel.ML_LOOP:
				elementCounters[from][toIdx][MULTI_LOOPS]++;
				break;
			case RNANodeLabel.HP_LOOP:
				elementCounters[from][toIdx][HAIRPINS]++;
				break;
			case RNANodeLabel.IL_LOOP:
				elementCounters[from][toIdx][INTERNAL_LOOPS]++;
				break;
			case RNANodeLabel.EXTERNAL:
				elementCounters[from][toIdx][EXTERNAL_LOOP]++;
				break;
			case RNANodeLabel.ROOT:
				elementCounters[from][toIdx][ROOT]++;
				break;
			}			
		}
	}

	private void clearElementsToPool(int[][][] elementCounters){
		if (elementCounters == null){
			return;
		}
		// return the arrays to the pool
		for (int i = 0; i < elementCounters.length; i++) {
			for (int j = 0; j < elementCounters[i].length; j++) {
				if (elementCounters[i][j] != null){
					Arrays.fill(elementCounters[i][j], 0);
					pool.enqueue(elementCounters[i][j]);				
					elementCounters[i][j] = null;
				}
			}
		}
	}

	public void calculateMatchCosts(Tree t,Tree s){
		//clear old information to pool
		clearElementsToPool(elementCountersT);
		clearElementsToPool(elementCountersS);
		
		calculateTreeCounters(t, elementCountersT);
		calculateTreeCounters(s, elementCountersS);
		
		
	}
}
