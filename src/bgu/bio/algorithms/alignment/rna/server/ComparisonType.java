package bgu.bio.algorithms.alignment.rna.server;

public enum ComparisonType{
	ORDERING, ROOTING,UNROOTED_ORDERING,UNORDERED_ROOTING,TIMING,UNROOTED_UNORDERED
	,ALL, RO_RU_UU,EVD;
}