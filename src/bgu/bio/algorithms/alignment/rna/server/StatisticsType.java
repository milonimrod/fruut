package bgu.bio.algorithms.alignment.rna.server;


public enum StatisticsType {
	NONE,EVAL,PVAL;
	
	
	public static StatisticsType parse(String str) {
		if ("eval".equalsIgnoreCase(str)) {
			return EVAL;
		} else if ("pval".equalsIgnoreCase(str)) {
			return PVAL;
		} else {
			return NONE;
		}
	}
}
