package bgu.bio.algorithms.alignment.rna.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.prefs.Preferences;

public class StatusChecker {
	private PrintWriter out = null;
	private BufferedReader networkIn = null;
	private Socket theSocket;
	private boolean closed;
	private String serverName;
	private int port;

	public boolean open(String serverName, int port) {

		try {
			theSocket = new Socket(serverName, port);
			networkIn = new BufferedReader(new InputStreamReader(
					theSocket.getInputStream()));
			out = new PrintWriter(theSocket.getOutputStream());

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		this.serverName = serverName;
		this.port = port;
		this.closed = false;
		return true;
	}

	public boolean close() {
		this.closed = true;
		try {
			String theLine = "{\"command\":\"terminate\"}";
			out.println(theLine);
			out.flush();
			theLine = networkIn.readLine();
			System.out.println("Server answer: " + theLine);
			if (networkIn != null)
				networkIn.close();
			if (out != null)
				out.close();
			return true;
		} catch (IOException e) {
			return false;
		}
	}

	public static void main(String[] args) {
		StatusChecker obs = new StatusChecker();
		Scanner sc = new Scanner(System.in);
		Preferences prefs = Preferences.userNodeForPackage(StatusChecker.class);
		String serverName = prefs.get("server", null);
		int port = prefs.getInt("port", -1);
		boolean dontAsk = false;
		if (serverName != null && port != -1) {
			System.out.print("Load defaults ? : [Y/N]");
			String s = sc.next();
			if (s.toLowerCase().equals("y")) {
				dontAsk = true;
			}
		}
		if (!dontAsk) {
			System.out.print("Enter the server name: ");
			serverName = sc.next();
			System.out.print("Enter port: ");
			port = sc.nextInt();
		}
		prefs.put("server", serverName);
		prefs.putInt("port", port);
		obs.open(serverName, port);
		String lastFile = prefs.get("lastFile", "file.csv");
		boolean contin = true;
		while (contin) {
			System.out.println("Choose a command:");
			System.out.println(1 + " - results to file");
			System.out.println(2 + " - number of results");
			System.out.println(3 + " - computation time");
			System.out.println(0 + " - exit");
			int command = -1;
			try {
				command = Integer.parseInt(sc.next());
			} catch (Exception e) {
				System.out.println("Unknown command");
			}
			switch (command) {
			case 0:
				contin = false;
				obs.close();
				break;
			case 1:
				try {
					System.out.print("FileName [or . for " + lastFile + "] :");
					String ans = sc.next();
					if (ans != null && ans.trim().length() != 1) {
						lastFile = ans;
						prefs.put("lastFile", lastFile);
					}
					obs.printStatus(lastFile, 3);
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case 2:
				try {
					obs.getStats(3);
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case 3:
				try {
					obs.reportTime(3);
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;

			}
		}
		sc.close();
	}

	public void reportTime(int tries) {
		if (tries < 0)
			return;
		try {
			if (closed || this.theSocket == null || this.theSocket.isClosed()) {
				open(serverName, port);
			}
			String theLine = "{\"command\":\"get_times\"}";
			out.println(theLine);
			out.flush();
			theLine = networkIn.readLine();
			System.out.println("Server answer: " + theLine);
		} catch (IOException e) {

			reportTime(tries - 1);
		}
	}

	public void getStats(int tries) {
		if (tries < 0)
			return;
		try {
			if (closed || this.theSocket == null || this.theSocket.isClosed()) {
				open(serverName, port);
			}
			String theLine = "{\"command\":\"status\"}";
			out.println(theLine);
			out.flush();
			theLine = networkIn.readLine();
			System.out.println("Server answer: " + theLine);

		} catch (IOException e) {
			getStats(tries - 1);
		}
	}

	public void printStatus(String fileName, int tries) {
		if (tries < 0)
			return;
		try {
			if (closed || this.theSocket == null || this.theSocket.isClosed()) {
				open(serverName, port);
			}
			String theLine = "{\"command\":\"results\",\"file\":\"" + fileName
					+ "\"}";
			out.println(theLine);
			out.flush();
			theLine = networkIn.readLine();
			System.out.println("Server answer: " + theLine);

		} catch (IOException e) {
			printStatus(fileName, tries - 1);
		}
	}
}
