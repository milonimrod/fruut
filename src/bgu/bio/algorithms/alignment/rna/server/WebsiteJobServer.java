package bgu.bio.algorithms.alignment.rna.server;

import gnu.trove.list.array.TIntArrayList;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.XMLFormatter;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.RealDistribution;

import bgu.bio.adt.queue.BlockingQueue;
import bgu.bio.adt.rna.RNANodeLabel;
import bgu.bio.adt.rna.RNASpecificTree;
import bgu.bio.adt.rna.costs.RnaSpecificCostFunction;
import bgu.bio.adt.rna.costs.RnaSpecificSmoothCost;
import bgu.bio.adt.rna.costs.RnaSpecificTypeRelatedTreePruning;
import bgu.bio.algorithms.alignment.AffineGapGlobalSequenceAlignmentNoMatrix;
import bgu.bio.algorithms.alignment.SequenceAlignment;
import bgu.bio.algorithms.alignment.rna.EngineType;
import bgu.bio.algorithms.alignment.rna.server.protocol.WebsiteJobProtocol;
import bgu.bio.algorithms.alignment.rna.server.protocol.WebsiteJobProtocolData;
import bgu.bio.algorithms.graphs.costs.NodeSmoothingCostCalculator;
import bgu.bio.algorithms.graphs.hsa.HSA;
import bgu.bio.algorithms.graphs.hsa.matchers.MatcherFactory;
import bgu.bio.algorithms.graphs.hsa.matchers.OrderedMatcherFactory;
import bgu.bio.algorithms.graphs.hsa.matchers.UnorderedMatcherFactory;
import bgu.bio.com.protocol.AsyncServerProtocol;
import bgu.bio.com.protocol.ServerProtocolFactory;
import bgu.bio.com.reactor.Reactor;
import bgu.bio.com.sql.MySqlConnector;
import bgu.bio.io.file.json.JSONException;
import bgu.bio.io.file.json.JSONObject;
import bgu.bio.mail.SendMailBGU;
import bgu.bio.util.AffineGapScoringMatrix;
import bgu.bio.util.ScoringMatrix;
import bgu.bio.util.alphabet.RNABasePairAlphabet;
import bgu.bio.util.alphabet.RnaAlphabet;

public class WebsiteJobServer {

	/** The threads. */
	private LinkedList<Thread> threads;
	private Logger log;
	private BlockingQueue<JSONObject> pendingJobs;
	private MySqlConnector conn;
	private Reactor reactor;

	public static void main(String[] args) throws FileNotFoundException,
			IOException {
		Properties prop = new Properties();
		prop.load(new FileInputStream(args[0]));

		WebsiteJobServer mainframe = new WebsiteJobServer(prop, "");
		mainframe.start();
	}

	/**
	 * Instantiates a new job management server.
	 * 
	 * @param props
	 *            the props
	 * @param prefix
	 *            the prefix
	 */
	public WebsiteJobServer(Properties props, String prefix) {

		// pad a dot if non exist and only if the prefix is given
		if (prefix == null)
			prefix = "";

		if (prefix.length() > 0 && prefix.endsWith(".")) {
			prefix += ".";
		}

		// Initiate the logger
		log = Logger.getLogger(props.getProperty(prefix + "log.class"));
		try {
			FileHandler fh = new FileHandler(props.getProperty(prefix
					+ "log.filename"));
			fh.setFormatter(new XMLFormatter());
			log.addHandler(fh);
		} catch (IOException e) {
			e.printStackTrace();
		}

		conn = new MySqlConnector(log);

		conn.connect(props.getProperty(prefix + "sql.server"),
				props.getProperty(prefix + "sql.db"),
				props.getProperty(prefix + "sql.user"),
				props.getProperty(prefix + "sql.password"));

		// update latest version in the SQL server
		final String version = getVersion();
		System.out.println("Running version: " + version);
		conn.runDML("UPDATE `site_versions` SET `version`='" + version
				+ "' WHERE `module`='fruut'");

		threads = new LinkedList<Thread>();
		// Load the reactor
		int port = Integer
				.parseInt(props.getProperty(prefix + "port", "57990"));
		int poolSize = Integer.parseInt(props.getProperty(prefix + "pool.size",
				"" + Runtime.getRuntime().availableProcessors()));

		pendingJobs = new BlockingQueue<JSONObject>();
		final WebsiteJobProtocolData data = new WebsiteJobProtocolData(
				pendingJobs);
		data.log = log;
		data.conn = conn;

		data.dists = buildDists(props);

		ServerProtocolFactory protocol = new ServerProtocolFactory() {
			@Override
			public AsyncServerProtocol create() {
				return new WebsiteJobProtocol(data);
			}
		};
		int runners = Integer.parseInt(props.getProperty(prefix
				+ "runners.size", ""
				+ Runtime.getRuntime().availableProcessors()));
		log.info("Loading " + runners + " runners");

		reactor = new Reactor(port, poolSize, protocol);

		// run the reactor as a thread
		Thread reactorThread = new Thread(reactor, "Reactor");

		threads.add(reactorThread);

		for (int i = 0; i < runners; i++) {
			threads.add(new Thread(new WebsiteJobRunner(data), "Worker" + i));
		}
	}

	private HashMap<EngineType, RealDistribution> buildDists(Properties props) {
		HashMap<EngineType, RealDistribution> ans = new HashMap<EngineType, RealDistribution>();
		String[] types = props.getProperty("types", "RO,RU,UO,UU").split(",");
		for (String string : types) {
			string = string.trim();
			double mu = Double.parseDouble(props.getProperty("stats." + string
					+ ".mu", "0"));
			double sigma = Double.parseDouble(props.getProperty("stats."
					+ string + ".sigma", "1"));
			ans.put(EngineType.valueOf(string), new NormalDistribution(mu,
					sigma));
		}
		return ans;
	}

	private String getVersion() {
		StringBuffer fileData = new StringBuffer(100);
		try {

			BufferedReader reader = new BufferedReader(new FileReader(
					"config/version.txt"));
			char[] buf = new char[1024];
			int numRead = 0;
			while ((numRead = reader.read(buf)) != -1) {
				String readData = String.valueOf(buf, 0, numRead);
				fileData.append(readData);
				buf = new char[1024];
			}
			reader.close();
		} catch (Exception e) {
			fileData.append("CAN'T READ VERSION");
		}
		return fileData.toString().replace("\r", "").replace("\n", "");
	}

	/**
	 * Start.
	 */
	public void start() {
		for (Thread t : threads) {
			t.start();
		}

		// observer wait until done
		Scanner sc = new Scanner(System.in);
		while (true) {
			String str = sc.next();
			if (str.equalsIgnoreCase("close")) {
				reactor.stopReactor();
				for (int i = 1; i < threads.size(); i++) {
					threads.get(i).interrupt();
				}
				break;
			}
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		sc.close();
		System.out.println("Done.");
	}

	private class WebsiteJobRunner implements Runnable {
		private MatcherFactory orderedFactory;
		private MatcherFactory unOrderedFactory;
		private HSA hsaEngine;
		private RnaSpecificCostFunction cost;

		private RnaSpecificTypeRelatedTreePruning pruneCostGlobal;
		private RnaSpecificTypeRelatedTreePruning pruneCostLocal;
		private NodeSmoothingCostCalculator smoothCost;
		private WebsiteJobProtocolData data;

		public WebsiteJobRunner(WebsiteJobProtocolData data) {
			this.data = data;
			pruneCostGlobal = new RnaSpecificTypeRelatedTreePruning(false);
			pruneCostLocal = new RnaSpecificTypeRelatedTreePruning(true);
			smoothCost = new RnaSpecificSmoothCost();

			orderedFactory = new OrderedMatcherFactory();
			unOrderedFactory = new UnorderedMatcherFactory();
			SequenceAlignment aligner = new AffineGapGlobalSequenceAlignmentNoMatrix(
					10, 10, RnaAlphabet.getInstance(),
					new AffineGapScoringMatrix("config" + File.separator
							+ "matrix" + File.separator
							+ "interval-RIBOSUM85-60.matrix",
							RnaAlphabet.getInstance()));

			this.cost = new RnaSpecificCostFunction(new ScoringMatrix("config"
					+ File.separator + "matrix" + File.separator
					+ "bp-RIBOSUM85-60.matrix",
					RNABasePairAlphabet.getInstance()), aligner);
			hsaEngine = new HSA(cost, unOrderedFactory);
		}

		@Override
		public void run() {
			JSONObject job = pendingJobs.dequeue();
			while (job != null) {
				try {
					String id = job.getString("id");

					final boolean ordered = job.optBoolean("Ordered", false);
					final boolean local = job.optBoolean("Local", false);
					final boolean rooted = job.optBoolean("Rooted", false);
					final boolean removeDang = job.optBoolean("RemoveDangling",
							false);
					final String jobType = job.optString("JobType", "Pair")
							.toLowerCase();
					final String rootDir = job.getString("RootDir")
							+ File.separator + job.getString("TreeDir")
							+ File.separator;
					String mailAddress = job.optString("mail", "").trim();
					if (mailAddress.equals("")) {
						mailAddress = null;
					}

					boolean done = false;
					String errorString = "";

					// set job status to running
					conn.runDML("UPDATE treejob SET status="
							+ WebsiteJobProtocol.JOB_RUNNING + " WHERE name='"
							+ id + "'");

					try {
						if (jobType.equals("pair")) {
							runPairJob(job, id, ordered, local, rooted,
									removeDang, rootDir);
						} else {
							runBatchJob(job, id, ordered, local, rooted,
									removeDang, rootDir);
						}
						done = true;

					} catch (Throwable e) {
						ByteArrayOutputStream os = new ByteArrayOutputStream();
						e.printStackTrace(new PrintStream(os));
						log.severe("Got Exception in run: " + e.getMessage()
								+ os.toString());
						errorString = e.getMessage() + "\n" + os.toString();
						done = false;
					}

					int status = done ? WebsiteJobProtocol.JOB_DONE
							: WebsiteJobProtocol.JOB_ERROR;

					// update finish status
					conn.runDML("UPDATE treejob SET status=" + status
							+ ", error='" + errorString + "' WHERE name='" + id
							+ "'");

					if (mailAddress != null) {
						SendMailBGU mail = new SendMailBGU(
								"negevcb@cs.bgu.ac.il",
								mailAddress,
								"Your Job on the FRUUT Server",
								"job id is "
										+ id
										+ "\n Please click on the following link to get more information: "
										+ "http://www.cs.bgu.ac.il/~negevcb/FRUUT/output.php?id="
										+ id
										+ "\n\n Thank you\n,Prof. Michal Ziv-Ukelson research team");
						mail.send();
					}

				} catch (JSONException e) {
					System.out.println(e);
				}

				job = pendingJobs.dequeue();
			}
		}

		/**
		 * @param job
		 * @param id
		 * @param ordered
		 * @param local
		 * @param rooted
		 * @param removeDang
		 * @param rootDir
		 * @throws IOException
		 * @throws JSONException
		 */
		public void runPairJob(JSONObject job, String id,
				final boolean ordered, final boolean local,
				final boolean rooted, final boolean removeDang,
				final String rootDir) throws IOException, JSONException {
			Writer writer = new FileWriter(job.getString("RootDir")
					+ File.separator + id + ".json");
			writer.write(job.toString());
			writer.close();

			// select local / global property
			RnaSpecificTypeRelatedTreePruning pruneCost = local ? pruneCostLocal
					: pruneCostGlobal;

			RNASpecificTree t = new RNASpecificTree();

			String[] tVienna = job.getString("tree1vienna")
					.replace("#####$$$$#####", "\n").split("\\n");
			boolean parse = t.buildFromViennaFormat(tVienna[1].trim()
					.toCharArray(), tVienna[2].trim().toCharArray(), true,
					removeDang);
			t.setName(tVienna[0]);

			RNASpecificTree s = new RNASpecificTree();

			String[] sVienna = job.getString("tree2vienna")
					.replace("#####$$$$#####", "\n").split("\\n");
			boolean self = job.getString("tree2vienna").trim().length() == 0
					|| sVienna == null || sVienna.length != 3;
			if (!self) {
				parse = parse
						&& s.buildFromViennaFormat(sVienna[1].trim()
								.toCharArray(),
								sVienna[2].trim().toCharArray(), true,
								removeDang);
				s.setName(sVienna[0]);

				if (!parse) {
					throw new RuntimeException(
							"Given RNA is not in legal bracket format !");
				}
			} else {
				System.out.println("Self score");
				s = t;
			}

			// select rooting settings
			pruneCost.setRooted(rooted);

			smoothCost.calculateCost(t);
			pruneCost.calculateCost(t);
			t.groupByStructure();
			smoothCost.calculateCost(s);
			pruneCost.calculateCost(s);

			// select factory for ordering
			MatcherFactory factory = ordered ? orderedFactory
					: unOrderedFactory;
			hsaEngine.setFactory(factory);

			EngineType engineType = getType(rooted, factory);

			long time = System.currentTimeMillis();
			double ans = 0;
			double ansRel = 0;
			TIntArrayList[] traceback = new TIntArrayList[3];
			if (self) {
				ans = hsaEngine.computeHSA(t, t, traceback);
				ansRel = 1;
			} else {
				ans = hsaEngine.computeHSA(t, s, traceback);
				ansRel = 2
						* ans
						/ (hsaEngine.computeHSA(s, s) + hsaEngine.computeHSA(t,
								t));
			}
			System.out.println("time: " + (System.currentTimeMillis() - time));

			Thread thread = new Thread(new Runnable() {
				@Override
				public void run() {
					Runtime rt = Runtime.getRuntime();
					String[] args = null;
					Process p = null;
					try {
						args = new String[] { "./dot", "-Tpng", "-o",
								rootDir + "tree1.png", rootDir + "tree1.dot" };
						p = rt.exec(args);
						p.waitFor();
						args = new String[] { "./dot", "-Tpng", "-o",
								rootDir + "tree2.png", rootDir + "tree2.dot" };
						p = rt.exec(args);
						p.waitFor();
					} catch (Exception e) {
					}
				}
			});

			thread.start();

			// write the trees to the output directory
			t.toDotFile(rootDir + "tree1.dot", traceback[0].getQuick(0), false);
			s.toDotFile(rootDir + "tree2.dot", traceback[1].getQuick(0), false);

			// write traceback to file
			File f = new File(rootDir + "Output0");
			if (!f.exists()) {
				f.mkdir();
			}
			double pval = 1000;
			if (!self) {
				if (!local) {
					// Global alignment
					RealDistribution dist = data.dists.get(engineType);
					pval = 1 - dist.cumulativeProbability(ansRel);
					System.out.println("mu=" + dist.getNumericalMean()
							+ " sigma=" + dist.getNumericalVariance()
							+ " pval=" + pval);
				} else {
					// Local alignment
				}
			}
			writer = new FileWriter(rootDir + "Ranks.txt");
			writer.write("0: " + ans + ", " + ansRel + ", " + (pval) + "\n");
			writer.close();
			writer = new FileWriter(rootDir + File.separator + "Output0"
					+ File.separator + "pairs.txt");
			writer.write("# tree1 : tree2\n");
			for (int i = 0; i < traceback[0].size(); i++) {
				RNANodeLabel tNode = (RNANodeLabel) t.getLabel(traceback[0]
						.getQuick(i));
				RNANodeLabel sNode = (RNANodeLabel) s.getLabel(traceback[1]
						.getQuick(i));

				if (tNode.getType() == RNANodeLabel.BASE_INTERVAL
						|| tNode.getType() == RNANodeLabel.BASE_PAIR) {
					final int group = tNode.getGroup();
					String sep = tNode.getType() == RNANodeLabel.BASE_INTERVAL ? "-"
							: ",";
					writer.write(tNode.getSequenceIndStart() + sep
							+ tNode.getSequenceIndEnd());
					writer.write(" : ");
					writer.write(sNode.getSequenceIndStart() + sep
							+ sNode.getSequenceIndEnd());
					writer.write(" : ");
					writer.write("" + (group + 1));
					writer.write('\n');
				}
			}
			writer.close();
		}

		private EngineType getType(boolean rooted, MatcherFactory factory) {
			if (rooted) {
				return factory instanceof OrderedMatcherFactory ? EngineType.RO
						: EngineType.RU;
			}

			return factory instanceof OrderedMatcherFactory ? EngineType.UO
					: EngineType.UU;
		}

		public void runBatchJob(JSONObject job, String id, boolean ordered,
				boolean local, boolean rooted, boolean removeDang,
				String rootDir) throws IOException, JSONException {

			Writer writer = new FileWriter(job.getString("RootDir")
					+ File.separator + id + ".json");
			writer.write(job.toString());
			writer.close();

			// select local / global property
			RnaSpecificTypeRelatedTreePruning pruneCost = local ? pruneCostLocal
					: pruneCostGlobal;

			List<RNASpecificTree> trees = new ArrayList<RNASpecificTree>();

			String[] treesString = job.getString("trees")
					.replace("#####$$$$#####", "\n").split("\\n");
			String header = null;
			String seq = null;
			String str = null;
			for (int i = 0; i < treesString.length; i++) {
				final String line = treesString[i];
				if (seq != null && seq.length() > 0) {
					RNASpecificTree t = new RNASpecificTree();
					t.setName(header);
					t.buildFromViennaFormat(seq.trim().toCharArray(), str
							.trim().toCharArray());
				}
				header = line;
				i++;
				seq = treesString[i];
				i++;
				str = treesString[i];
			}

			for (int i = 0; i < trees.size(); i++) {
				RNASpecificTree t = trees.get(i);
				smoothCost.calculateCost(t);
				pruneCost.calculateCost(t);
				t.groupByStructure();

				for (int j = i + 1; j < trees.size(); j++) {
					RNASpecificTree s = trees.get(j);
					// select rooting settings
					pruneCost.setRooted(rooted);

					smoothCost.calculateCost(s);
					pruneCost.calculateCost(s);

					// select factory for ordering
					MatcherFactory factory = ordered ? orderedFactory
							: unOrderedFactory;
					hsaEngine.setFactory(factory);

					EngineType engineType = getType(rooted, factory);

					long time = System.currentTimeMillis();
					double ans = 0;
					double ansRel = 0;
					ans = hsaEngine.computeHSA(t, s);
					ansRel = 2
							* ans
							/ (hsaEngine.computeHSA(s, s) + hsaEngine
									.computeHSA(t, t));
					System.out.println("time: "
							+ (System.currentTimeMillis() - time));

					// write traceback to file
					File f = new File(rootDir + "Output0");
					if (!f.exists()) {
						f.mkdir();
					}
					double pval = 1000;
					if (!local) {
						// Global alignment
						RealDistribution dist = data.dists.get(engineType);
						pval = 1 - dist.cumulativeProbability(ansRel);
						System.out.println("mu=" + dist.getNumericalMean()
								+ " sigma=" + dist.getNumericalVariance()
								+ " pval=" + pval);
					} else {
						// Local alignment
					}
					writer = new FileWriter(rootDir + "Ranks.txt");
					writer.write("0: " + ans + ", " + ansRel + ", " + (pval)
							+ "\n");
					writer.close();
				}
			}
		}
	}
}
