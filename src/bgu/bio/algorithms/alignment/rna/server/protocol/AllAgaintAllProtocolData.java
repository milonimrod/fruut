package bgu.bio.algorithms.alignment.rna.server.protocol;

import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import bgu.bio.adt.rna.RNASpecificTree;
import bgu.bio.adt.tuples.IntPair;
import bgu.bio.algorithms.alignment.rna.provider.PairProvider;
import bgu.bio.algorithms.alignment.rna.server.ComparisonType;
import bgu.bio.io.file.json.JSONException;
import bgu.bio.io.file.json.JSONObject;

public class AllAgaintAllProtocolData {
	private List<TreeData> trees;
	private Object lock;
	private ComparisonType type;
	private List<float[]> results;
	private static NumberFormat scoreFormat = new DecimalFormat("#.00000E00");
	private int amount;
	private int counter;
	private long start;
	private ReentrantReadWriteLock resultLock;
	private ReentrantReadWriteLock timeLock;
	private int pairMultiplier;
	private int currentMultiplier;
	private TIntObjectHashMap<TIntIntHashMap> multiplierMap;
	private long totalComputationTime;
	private PairProvider provider;
	private IntPair currentPair;

	private static final int INDEX1 = 0;
	private static final int INDEX2 = 1;
	private static final int COUNTER = 2;
	private static final int SCORES = 3;
	private static final int MINIMAL_SIZE = 4;

	private static Map<ComparisonType, String[]> linkMap = new HashMap<ComparisonType, String[]>();
	private static Map<ComparisonType, Boolean> additionalInformation = new HashMap<ComparisonType, Boolean>();

	static {
		linkMap.put(
				ComparisonType.ROOTING,
				new String[] {
						"http://www.cs.bgu.ac.il/~negevcb/FRUUT/?tree1=#T1#nl&tree2=#T2#nl&Rooted=true&Ordered=true&Local=false",
						"http://www.cs.bgu.ac.il/~negevcb/FRUUT/?tree1=#T1#nl&tree2=#T2#nl&Rooted=false&Ordered=true&Local=false" });
		additionalInformation.put(ComparisonType.ROOTING, true);
		linkMap.put(
				ComparisonType.ORDERING,
				new String[] {
						"http://www.cs.bgu.ac.il/~negevcb/FRUUT/?tree1=#T1#nl&tree2=#T2#nl&Rooted=true&Ordered=true&Local=false",
						"http://www.cs.bgu.ac.il/~negevcb/FRUUT/?tree1=#T1#nl&tree2=#T2#nl&Rooted=true&Ordered=false&Local=false" });
		additionalInformation.put(ComparisonType.ORDERING, true);
		linkMap.put(
				ComparisonType.UNORDERED_ROOTING,
				new String[] {
						"http://www.cs.bgu.ac.il/~negevcb/FRUUT/?tree1=#T1#nl&tree2=#T2#nl&Rooted=true&Ordered=false&Local=false",
						"http://www.cs.bgu.ac.il/~negevcb/FRUUT/?tree1=#T1#nl&tree2=#T2#nl&Rooted=false&Ordered=false&Local=false" });
		additionalInformation.put(ComparisonType.UNROOTED_ORDERING, true);
		linkMap.put(
				ComparisonType.UNROOTED_ORDERING,
				new String[] {
						"http://www.cs.bgu.ac.il/~negevcb/FRUUT/?tree1=#T1#nl&tree2=#T2#nl&Rooted=false&Ordered=true&Local=false",
						"http://www.cs.bgu.ac.il/~negevcb/FRUUT/?tree1=#T1#nl&tree2=#T2#nl&Rooted=false&Ordered=false&Local=false" });
		additionalInformation.put(ComparisonType.UNROOTED_ORDERING, true);
		linkMap.put(
				ComparisonType.UNROOTED_UNORDERED,
				new String[] {
						"http://www.cs.bgu.ac.il/~negevcb/FRUUT/?tree1=#T1#nl&tree2=#T2#nl&Rooted=true&Ordered=true&Local=false",
						"http://www.cs.bgu.ac.il/~negevcb/FRUUT/?tree1=#T1#nl&tree2=#T2#nl&Rooted=false&Ordered=false&Local=false" });
		additionalInformation.put(ComparisonType.UNROOTED_UNORDERED, true);
		linkMap.put(
				ComparisonType.RO_RU_UU,
				new String[] {
						"http://www.cs.bgu.ac.il/~negevcb/FRUUT/?tree1=#T1#nl&tree2=#T2#nl&Rooted=true&Ordered=true&Local=false",
						"http://www.cs.bgu.ac.il/~negevcb/FRUUT/?tree1=#T1#nl&tree2=#T2#nl&Rooted=true&Ordered=false&Local=false",
						"http://www.cs.bgu.ac.il/~negevcb/FRUUT/?tree1=#T1#nl&tree2=#T2#nl&Rooted=false&Ordered=false&Local=false" });
		additionalInformation.put(ComparisonType.RO_RU_UU, true);

		linkMap.put(ComparisonType.TIMING, new String[] { "", "" });
		additionalInformation.put(ComparisonType.TIMING, false);

		linkMap.put(
				ComparisonType.ALL,
				new String[] {
						"http://www.cs.bgu.ac.il/~negevcb/FRUUT/?tree1=#T1#nl&tree2=#T2#nl&Rooted=true&Ordered=true&Local=false",
						"http://www.cs.bgu.ac.il/~negevcb/FRUUT/?tree1=#T1#nl&tree2=#T2#nl&Rooted=true&Ordered=false&Local=false",
						"http://www.cs.bgu.ac.il/~negevcb/FRUUT/?tree1=#T1#nl&tree2=#T2#nl&Rooted=false&Ordered=true&Local=false",
						"http://www.cs.bgu.ac.il/~negevcb/FRUUT/?tree1=#T1#nl&tree2=#T2#nl&Rooted=false&Ordered=false&Local=false" });
		additionalInformation.put(ComparisonType.ALL, true);

		linkMap.put(ComparisonType.EVD, new String[] { "", "", "", "" });
		additionalInformation.put(ComparisonType.EVD, false);
	}

	public AllAgaintAllProtocolData(ArrayList<RNASpecificTree> trees,
			PairProvider pairs, Properties props) {

		type = ComparisonType.valueOf(props.getProperty("type"));
		pairMultiplier = Math.max(1,
				Integer.parseInt(props.getProperty("multipler", "1")));

		// multiplier information
		this.currentMultiplier = pairMultiplier;
		this.multiplierMap = new TIntObjectHashMap<TIntIntHashMap>();
		this.trees = copyTreeData(trees);
		lock = new Object();
		resultLock = new ReentrantReadWriteLock(true);
		timeLock = new ReentrantReadWriteLock(true);
		results = new ArrayList<float[]>();

		counter = 0;
		this.provider = pairs;

		amount = provider.initialAmount();
		System.out.println("Running on " + trees.size()
				+ " trees. Total pairs equals to " + amount);

	}

	private List<TreeData> copyTreeData(ArrayList<RNASpecificTree> treesToCopy) {
		List<TreeData> list = new ArrayList<AllAgaintAllProtocolData.TreeData>();
		for (RNASpecificTree tree : treesToCopy) {
			TreeData tmp = new TreeData();
			tmp.name = tree.getName();
			tmp.n = tree.getNodeNum();
			tmp.l = tree.getNumberOfLeaves();
			tmp.d = tree.getMaxOutDeg();
			list.add(tmp);
		}
		return list;
	}

	public void nextPair(JSONObject obj) throws JSONException {
		synchronized (lock) {
			if (counter == 0) {
				start = System.currentTimeMillis();
			}
			counter++;

			if (currentPair == null) {
				currentPair = provider.hasNext() ? provider.next() : null;
			}

			if (currentPair != null) {
				obj.put("ind1", currentPair.getFirst());
				obj.put("ind2", currentPair.getSecond());
				this.currentMultiplier--;
				if (this.currentMultiplier == 0) {
					currentPair = null;
					this.currentMultiplier = this.pairMultiplier;
				}
			} else {
				obj.put("ind1", -1);
				obj.put("ind2", -1);

				resultLock.readLock().lock();
				final int resultsAmount = results.size();
				resultLock.readLock().unlock();
				System.out.println("Done. total of " + resultsAmount
						+ " results");
				return;
			}

			if (counter % 2000 == 0) {
				resultLock.readLock().lock();
				final int resultsAmount = results.size();
				resultLock.readLock().unlock();
				System.out.println("\ncounter = " + counter + " left: "
						+ (amount - counter) + " results: " + resultsAmount);
				final double current = System.currentTimeMillis() - start;
				System.out.println("Expected time: "
						+ time((int) (Math.ceil((amount - counter) * current
								/ (counter * 1000d)))) + " seconds.");
			}
		}
	}

	private String time(int s) {
		return String.format("%02d:%02d:%02d", s / 3600, (s % 3600) / 60,
				(s % 60));
	}

	public String getStatus() {
		resultLock.readLock().lock();
		final int resultsAmount = results.size();
		resultLock.readLock().unlock();
		return "counter = " + counter + " left: " + (amount - counter)
				+ " results: " + resultsAmount;
	}

	public void saveResult(JSONObject obj) throws JSONException {

		resultLock.writeLock().lock();
		if (this.pairMultiplier != 1) {
			// check if exist
			if (!multiplierMap.containsKey(obj.getInt("ind1"))) {
				multiplierMap.put(obj.getInt("ind1"), new TIntIntHashMap());
			}
			TIntIntHashMap level2 = multiplierMap.get(obj.getInt("ind1"));
			if (!level2.containsKey(obj.getInt("ind2"))) {

				final float[] res = buildResult(obj);

				results.add(res);
				level2.put(obj.getInt("ind2"), results.size() - 1);

			} else {

				// exist: just update the p-value information and the counter
				int index = level2.get(obj.getInt("ind2"));
				float[] res = results.get(index);
				res[COUNTER] += 1.0f;
				// if its the last time we see it, we can remove it from the map
				if ((int) res[COUNTER] == this.pairMultiplier) {
					level2.remove(obj.getInt("ind2"));
					if (level2.size() == 0) {
						// remove from first level
						multiplierMap.remove(obj.getInt("ind1"));
					}
				}

			}
		} else {
			final float[] res = buildResult(obj);
			results.add(res);
		}
		resultLock.writeLock().unlock();
		System.out.print(".");
	}

	/**
	 * @param obj
	 * @return
	 * @throws JSONException
	 */
	public float[] buildResult(JSONObject obj) throws JSONException {
		final int scores = obj.getInt("scores");
		final float[] res = new float[MINIMAL_SIZE + scores];
		res[INDEX1] = obj.getInt("ind1");
		res[INDEX2] = obj.getInt("ind2");
		res[COUNTER] = 1.0f;
		res[SCORES] = scores;
		for (int i = 0; i < scores; i++) {
			final double ans = Double.longBitsToDouble(obj.getLong("score"
					+ (i + 1)));
			res[MINIMAL_SIZE + i] = Double.isNaN(ans) ? Float.NaN : (float) ans;
		}
		return res;
	}

	public void reportTime(JSONObject obj) throws JSONException {
		timeLock.writeLock().lock();
		totalComputationTime += obj.optLong("time", 0l);
		timeLock.writeLock().unlock();
	}

	public void getResults(String fileName) {
		String[] links = new String[0];
		if (linkMap.containsKey(type)) {
			links = linkMap.get(type);
		}
		boolean additionalInfo = true;
		if (additionalInformation.containsKey(type)) {
			additionalInfo = additionalInformation.get(type).booleanValue();
		}

		StringBuilder sb = new StringBuilder();
		FileWriter fw = null;
		try {
			fw = new FileWriter(fileName);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		resultLock.readLock().lock();
		for (int r = 0; r < results.size(); r++) {
			// release every 2000 results
			if (r % 5000 == 0) {
				resultLock.readLock().unlock();
				Thread.yield();
				resultLock.readLock().lock();
			}
			float[] res = results.get(r);
			final int ind1 = (int) res[INDEX1];
			final int ind2 = (int) res[INDEX2];

			final TreeData tree1 = trees.get(ind1);
			final TreeData tree2 = trees.get(ind2);

			sb.append(tree1.name);
			sb.append(',');
			sb.append(tree2.name);
			final int scores = (int) res[SCORES];
			for (int i = 0; i < scores; i++) {
				sb.append(',');
				final double num = res[MINIMAL_SIZE + i];
				sb.append(Double.isInfinite(num) || Double.isNaN(num) ? "NaN"
						: scoreFormat.format(num));
			}
			if (additionalInfo) {
				sb.append(',');
				sb.append(tree1.n);
				sb.append(',');
				sb.append(tree2.n);
				sb.append(',');
				sb.append(tree1.l);
				sb.append(',');
				sb.append(tree2.l);
				sb.append(',');
				sb.append(tree1.d);
				sb.append(',');
				sb.append(tree2.d);
				// links
				for (int i = 0; i < links.length; i++) {
					sb.append(',');
					sb.append(links[i].replace("#T1#", tree1.name).replace(
							"#T2#", tree2.name));
				}
			}
			sb.append('\n');
			if (r % 5000 == 0) {
				try {
					fw.write(sb.toString());
				} catch (IOException ex) {

				}
				sb.setLength(0);
			}
		}
		resultLock.readLock().unlock();

		try {
			// write leftovers in the builder
			fw.write(sb.toString());
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public long saveTime() {
		this.timeLock.readLock().lock();
		final long ans = this.totalComputationTime;
		this.timeLock.readLock().unlock();
		return ans;
	}

	private class TreeData {
		String name;
		int n;
		int l;
		int d;
	}
}