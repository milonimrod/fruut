package bgu.bio.algorithms.alignment.rna.server.protocol;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;


import bgu.bio.algorithms.alignment.rna.server.protocol.WebsiteJobProtocolData;
import bgu.bio.com.protocol.AsyncServerProtocol;
import bgu.bio.io.file.json.JSONException;
import bgu.bio.io.file.json.JSONObject;

public class WebsiteJobProtocol implements AsyncServerProtocol {
	public static final int JOB_PENDING = 0;
	public static final int JOB_RUNNING = 1;
	public static final int JOB_DONE = 2;
	public static final int JOB_ERROR = -1;
	
	private boolean _shouldClose = false;
	private boolean _connectionTerminated = false;
	private final WebsiteJobProtocolData data;

	public WebsiteJobProtocol(WebsiteJobProtocolData data) {
		this.data = data;
	}

	@Override
	public void connectionTerminated() {
		this._connectionTerminated = true;
	}

	@Override
	public boolean isEnd(String msg) {
		try{
			JSONObject obj = new JSONObject(msg);
			String command = obj.getString("command");
			return command.equals("terminate");
		} catch(JSONException ex){
			ex.printStackTrace();
		}
		return false;
	}

	@Override
	public String processMessage(String msg) {
		try{
			if (this._connectionTerminated) {
				return "{\"message\":\"TERM\"}";
			}
			if (this.isEnd(msg)) {
				this._shouldClose = true;
				return "{\"message\":\"TERM\"}";
			}
			JSONObject obj = new JSONObject(msg);

			String command = obj.getString("command");
			if(command.equals("register.job"))
			{
				String id = obj.getString("id");
				data.log.info("got job " + id  + " "  + obj.toString());
				//register the job in the DB
				data.conn.runDML("INSERT INTO treejob (name,status) VALUES ('"+id+"',"+JOB_PENDING+")");
				//put the job in the queue
				data.pendingJobs.enqueue(obj);
				return "{\"command\":\"OK\"}";
			}
			this.data.log.info("Mainframe got a unknown command " + msg);
			return "{\"command\":\"error\",\"reason\":\"unknown command\"}";

		}catch(JSONException ex){
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			ex.printStackTrace(new PrintStream(os));
			data.log.severe("Got JSON Exception in run: " + os.toString());
			return "{\"command\":\"error\",\"reason\":\"JSON format error\"}";
		}
	}

	@Override
	public boolean shouldClose() {
		return _shouldClose;
	}

}
