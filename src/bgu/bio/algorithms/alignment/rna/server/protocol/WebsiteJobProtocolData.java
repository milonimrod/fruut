package bgu.bio.algorithms.alignment.rna.server.protocol;

import java.util.HashMap;
import java.util.logging.Logger;

import org.apache.commons.math3.distribution.RealDistribution;

import bgu.bio.adt.queue.BlockingQueue;
import bgu.bio.algorithms.alignment.rna.EngineType;
import bgu.bio.com.sql.MySqlConnector;
import bgu.bio.io.file.json.JSONObject;

public class WebsiteJobProtocolData {
	public Logger log;
	public BlockingQueue<JSONObject> pendingJobs;
	public MySqlConnector conn;
	public HashMap<EngineType, RealDistribution> dists;
	
	public WebsiteJobProtocolData(BlockingQueue<JSONObject> pendingJobs) {
		
		this.pendingJobs = pendingJobs;
	}
}
