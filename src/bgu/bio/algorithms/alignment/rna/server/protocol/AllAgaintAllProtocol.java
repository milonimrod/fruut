package bgu.bio.algorithms.alignment.rna.server.protocol;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import bgu.bio.com.protocol.AsyncServerProtocol;
import bgu.bio.io.file.json.JSONException;
import bgu.bio.io.file.json.JSONObject;

public class AllAgaintAllProtocol implements AsyncServerProtocol {
	
	private boolean _shouldClose = false;
	private boolean _connectionTerminated = false;
	private final AllAgaintAllProtocolData data;

	public AllAgaintAllProtocol(AllAgaintAllProtocolData data) {
		this.data = data;
	}

	@Override
	public void connectionTerminated() {
		this._connectionTerminated = true;
	}

	@Override
	public boolean isEnd(String msg) {
		try{
			JSONObject obj = new JSONObject(msg);
			String command = obj.getString("command");
			return command.equals("terminate");
		} catch(JSONException ex){
			ex.printStackTrace();
		}
		return false;
	}

	@Override
	public String processMessage(String msg) {
		try{
			if (this._connectionTerminated) {
				return "{\"message\":\"TERM\"}";
			}
			if (this.isEnd(msg)) {
				this._shouldClose = true;
				return "{\"message\":\"TERM\"}";
			}
			JSONObject obj = new JSONObject(msg);

			String command = obj.getString("command");
			if(command.equals("next"))
			{
				JSONObject ans = new JSONObject();
				ans.put("message", "OK");
				data.nextPair(ans);
				return ans.toString();
			}else if (command.equals("update")){
				data.saveResult(obj);
				return "{\"message\":\"OK\"}";
			
			}else if (command.equals("report_time")){
				data.reportTime(obj);
				return "{\"message\":\"OK\"}";
			}else if (command.equals("get_times")){
				long time = data.saveTime();
				return "{\"message\":\"OK\" "+"\"time\":"+time+"}";
			}else if (command.equals("results")){
				data.getResults(obj.getString("file"));
				return "{\"message\":\"OK\"}";

			}else if (command.equals("status")){
				JSONObject ans = new JSONObject();
				ans.put("message", "OK");
				ans.put("response", data.getStatus());				
				return ans.toString();
			}
			return "{\"command\":\"UNKNOWN\"}";
		}catch(JSONException ex){
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			ex.printStackTrace(new PrintStream(os));
			return "{\"command\":\"error\",\"reason\":\"JSON format error\"}";
		}
	}

	@Override
	public boolean shouldClose() {
		return _shouldClose;
	}

}
