package bgu.bio.algorithms.alignment.rna.server;

import gnu.trove.map.hash.TIntIntHashMap;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

import bgu.bio.adt.rna.RNASpecificTree;
import bgu.bio.adt.rna.filter.RNATreeFilter;
import bgu.bio.adt.tuples.IntPair;
import bgu.bio.algorithms.alignment.rna.provider.PairProvider;
import bgu.bio.algorithms.alignment.rna.server.protocol.AllAgaintAllProtocol;
import bgu.bio.algorithms.alignment.rna.server.protocol.AllAgaintAllProtocolData;
import bgu.bio.com.protocol.AsyncServerProtocol;
import bgu.bio.com.protocol.ServerProtocolFactory;
import bgu.bio.com.reactor.Reactor;
import bgu.bio.io.file.properties.PropertiesUtils;
import bgu.bio.util.Filter;

public class AllAgaintAllServer {

	/** The threads. */
	private LinkedList<Thread> threads;
	private Reactor reactor;

	public static void main(String[] args) throws FileNotFoundException,
	IOException, NumberFormatException,
	InstantiationException, IllegalAccessException {
		Properties prop = new Properties();
		prop.load(new FileInputStream(args[0]));

		AllAgaintAllServer mainframe = new AllAgaintAllServer(prop, "");
		System.out.println("Starting ...");
		mainframe.start();
		System.out.println("Joining the threads");
		mainframe.startObserver();
	}

	/**
	 * Instantiates a new job management server.
	 * 
	 * @param props
	 *            the props
	 * @param prefix
	 *            the prefix
	 * @throws IOException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws CTException
	 * @throws NumberFormatException
	 */
	public AllAgaintAllServer(Properties props, String prefix) {

		// pad a dot if non exist and only if the prefix is given
		if (prefix == null)
			prefix = "";

		if (prefix.length() > 0 && prefix.endsWith(".")) {
			prefix += ".";
		}
		
		RNATreeFilter filter = null;
		try {
			String[] sp = props.getProperty("filter").split("\\|");
			String name = sp[0];
			String params = sp.length == 2 ? sp[1] : null;			
			filter = (RNATreeFilter)PropertiesUtils.initiateFromProps(name, params, props);
		} catch (IOException e) {
			e.printStackTrace();
		}

		String[] input = props.getProperty("input").split(",");
		ArrayList<RNASpecificTree> trees = new ArrayList<RNASpecificTree>();

		ComparisonType type = ComparisonType.valueOf(props.getProperty("type"));		
		System.out.println("type=" + type);
		System.out.println("inputs=" + Arrays.toString(input));
		List<List<RNASpecificTree>> lists = loadTrees(input,filter, trees);
		System.out.println("Size histogram");
		TIntIntHashMap sizeMap = new TIntIntHashMap();
		for (List<RNASpecificTree> subList : lists){
			for (RNASpecificTree rnaSpecificTree : subList) {
				if (!sizeMap.containsKey(rnaSpecificTree.getNodeNum())){
					sizeMap.put(rnaSpecificTree.getNodeNum(), 1);
				}else{
					sizeMap.put(rnaSpecificTree.getNodeNum(), sizeMap.get(rnaSpecificTree.getNodeNum()) + 1);
				}
			}
		}
		
		int[] keys = Arrays.copyOf(sizeMap.keys(), sizeMap.size());
		Arrays.sort(keys);
		for (int i=0;i < keys.length;i++){
			int num = keys[i];
			System.out.println(num + " , " + sizeMap.get(num));
		}
		//List<List<RNASpecificTree>> lists = loadTrees(input, new FilterPartialRNAsePTrees(120, 172), trees);
		printStats(trees);
		System.out.println("Loaded " + trees.size() + " trees");
		/*
		if (props.containsKey("pairs.file")) {
			mapNamesToIdsAndCreatePairs(props, trees, pairs);
		}
		*/

		threads = new LinkedList<Thread>();

		PairProvider provider = null;
		try {
			String[] sp = props.getProperty("provider").split("\\|");
			String name = sp[0];
			String params = sp.length == 2 ? sp[1] : null;			
			provider = (PairProvider)PropertiesUtils.initiateFromProps(name, params, props);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.print("Initialaizing the provider .. ");
		provider.initiate(lists, true);
		System.out.println("Done.");
		final AllAgaintAllProtocolData data = new AllAgaintAllProtocolData(trees,provider, props);
		trees.clear();
		ServerProtocolFactory protocol = new ServerProtocolFactory() {
			@Override
			public AsyncServerProtocol create() {
				return new AllAgaintAllProtocol(data);
			}
		};

		// Load the reactor
		int port = Integer
				.parseInt(props.getProperty(prefix + "port", "54111"));
		int poolSize = Integer.parseInt(props.getProperty(prefix + "pool", ""
				+ Runtime.getRuntime().availableProcessors()));
		reactor = new Reactor(port, poolSize, protocol);

		// run the reactor as a thread
		Thread reactorThread = new Thread(reactor, "Reactor");

		threads.add(reactorThread);
	}

	private void printStats(ArrayList<RNASpecificTree> trees) {
		double leafsSum = 0;
		double degSum = 0;
		double sizeSum = 0;
		for (RNASpecificTree tree : trees) {
			leafsSum += tree.getNumberOfLeaves();
			sizeSum += tree.getNodeNum();
			degSum += tree.getMaxOutDeg();			
		}
		
		System.out.println("AVG leafs in tree: " + leafsSum/trees.size());
		System.out.println("AVG nodes in tree: " + sizeSum/trees.size());
		System.out.println("AVG max degree in trees: " + degSum/trees.size());
	}

	/**
	 * @param props
	 * @param minimalSize
	 * @param minimalDegree
	 * @param input
	 * @param pairs
	 * @param type
	 * @return
	 */
	public static List<List<RNASpecificTree>> loadTrees(String[] inputs,
			Filter<RNASpecificTree> filter, ArrayList<RNASpecificTree> trees) {
		List<List<RNASpecificTree>> ans = new ArrayList<List<RNASpecificTree>>();
		for (String fName : inputs){
			File input = new File(fName);
			if (input.isFile()) {
				ArrayList<RNASpecificTree> temp = RNASpecificTree.loadFromFile(input.getAbsolutePath(),filter,true);
				ans.add(temp);
				trees.addAll(temp);
			} else {				
				File[] fileNames = input.listFiles(new FilenameFilter() {

					@Override
					public boolean accept(File dir, String name) {
						return name.toLowerCase().endsWith(".fasta") || name.toLowerCase().endsWith(".fa");
					}
				});
				for (int f = 0; f < fileNames.length; f++) {
					ArrayList<RNASpecificTree> temp = RNASpecificTree.loadFromFile(
							fileNames[f].getAbsolutePath(), filter);
					ans.add(temp);
					trees.addAll(temp);
				}
			}
		}
		
		//set id's
		int id = 0;
		for (List<RNASpecificTree> inner : ans){
			for (RNASpecificTree tree : inner){
				tree.setId(id);
				id++;
			}
		}		
		return ans;
	}

	/**
	 * @param props
	 * @param trees
	 * @param pairs
	 * @param nameMap
	 */
	private void mapNamesToIdsAndCreatePairs(Properties props,
			ArrayList<RNASpecificTree> trees, ArrayList<IntPair> pairs) {
		HashMap<String, Integer> nameMap = new HashMap<String, Integer>();
		int pos = 0;
		for (RNASpecificTree rnaSpecificTree : trees) {
			nameMap.put(rnaSpecificTree.getName().toLowerCase(), pos);
			pos++;
		}
		try {
			FileReader file = new FileReader(new File(
					props.getProperty("pairs.file")));
			BufferedReader reader = new BufferedReader(file);

			String line = reader.readLine();
			while (line != null) {
				String[] sp = line.toLowerCase().split(",");
				String name1 = sp[0].trim();
				String name2 = sp[1].trim();
				pairs.add(new IntPair(nameMap.get(name1), nameMap
						.get(name2)));
				line = reader.readLine();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		Collections.reverse(pairs);
	}

	/**
	 * Start.
	 */
	public void start() {		
		for (Thread t : threads) {
			t.start();
		}
	}

	public void startObserver(){
		// observer wait until done
		Scanner sc = new Scanner(System.in);
		while (true) {
			String str = sc.next();
			if (str.equalsIgnoreCase("close")) {
				stop();
				break;
			}
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Done.");
	}

	public void stop() {
		reactor.stopReactor();
		for (int i = 1; i < threads.size(); i++) {
			threads.get(i).interrupt();
		}
	}
}
