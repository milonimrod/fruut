package bgu.bio.algorithms.alignment.rna.server;

import bgu.bio.adt.rna.RNASpecificTree;
import bgu.bio.util.Filter;

public class FilterPartialRNAsePTrees implements Filter<RNASpecificTree> {

	private int min;
	private int max;

	public FilterPartialRNAsePTrees(int min, int max) {
		this.min = min;
		this.max = max;
	}

	@Override
	public boolean shouldPass(RNASpecificTree obj) {
		return !(obj.getNodeNum() >= min && obj.getNodeNum() <= max);
	}

}
