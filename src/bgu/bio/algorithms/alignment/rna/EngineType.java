package bgu.bio.algorithms.alignment.rna;

import bgu.bio.algorithms.graphs.hsa.HSA;

/**
 * An ENUM to hold information about the types of {@link HSA} engines (e.g. Rooted-ordered). 
 * @author milon
 *
 */
public enum EngineType {
	RO,RU,UO,UU;
}
