package bgu.bio.algorithms.alignment.rna.server;

import static org.junit.Assert.fail;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import bgu.bio.algorithms.alignment.rna.WorkerRNATreeHSA;
import bgu.bio.algorithms.alignment.rna.server.AllAgaintAllServer;
import bgu.bio.io.file.json.JSONException;

public class TestServerClient {

	private static final String CONFIG_FILE = "config/tests/enumEVD-Local.props";
	private static final int THREADS = 3;
	private AllAgaintAllServer server;


	@Before
	public void start(){
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(CONFIG_FILE));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		server = new AllAgaintAllServer(prop, "");
		server.start();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test() {
		Assume.assumeTrue(Runtime.getRuntime().availableProcessors() >= 2);
		Runnable run = new Runnable() {

			@Override
			public void run() {
				try {
					WorkerRNATreeHSA.main(new String[]{CONFIG_FILE});
				} catch (FileNotFoundException e) {
					fail("File not Found");
					e.printStackTrace();
				} catch (IOException e) {
					fail("Error in IO !");
					e.printStackTrace();
				} catch (JSONException e) {
					fail("Error in parsing a JSON response");
					e.printStackTrace();
				}
			}
		};

		ArrayList<Thread> list = new ArrayList<Thread>();
		for (int i=0;i<THREADS;i++){
			Thread t = new Thread(run);
			list.add(t);
			t.start();

		}
		for (int i=0;i<list.size();i++){
			Thread thread = list.get(i);
			try {
				thread.join();
			} catch (InterruptedException e) {
			}
		}

	}

	@After
	public void stop(){
		server.stop();
	}

}
