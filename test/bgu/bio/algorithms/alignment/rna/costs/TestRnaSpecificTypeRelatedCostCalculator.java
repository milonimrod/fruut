package bgu.bio.algorithms.alignment.rna.costs;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import bgu.bio.adt.rna.RNASpecificTree;
import bgu.bio.adt.rna.costs.RnaSpecificTypeRelatedTreePruning;


public class TestRnaSpecificTypeRelatedCostCalculator {
	@Test
	public void testSimple1(){
		RNASpecificTree t = new RNASpecificTree();
		t.buildFromViennaFormat(
				"CGACAGGAAACUGACAAACCCUUUCAUCUUAA".toCharArray(),
				"...((((...))...(((...)))..))....".toCharArray());
		
		//draw the tree in the temp directory
		try {
			t.toDotFile("/home/milon/tmp/1.dot", true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		RnaSpecificTypeRelatedTreePruning cost = new RnaSpecificTypeRelatedTreePruning(false);
		cost.calculateCost(t);
		
		Assert.assertEquals(96.5, t.getWeight(10, t.getNeighborIx(10, 5)),0.01);
		Assert.assertEquals(42.5, t.getWeight(6, t.getNeighborIx(6, 7)),0.01);
		
		cost.setRooted(true);
		cost.calculateCost(t);
		Assert.assertEquals(Double.POSITIVE_INFINITY, t.getWeight(10, t.getNeighborIx(10, 5)),0.01);
	}
}
