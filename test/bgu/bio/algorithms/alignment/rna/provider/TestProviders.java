package bgu.bio.algorithms.alignment.rna.provider;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bgu.bio.adt.rna.RNASpecificTree;
import bgu.bio.adt.rna.filter.FilterBySizeAndDegree;
import bgu.bio.adt.tuples.IntPair;
import bgu.bio.algorithms.alignment.rna.provider.AllAgainstAllProvider;
import bgu.bio.algorithms.alignment.rna.provider.EvalProvider;
import bgu.bio.algorithms.alignment.rna.provider.InnerListProvider;
import bgu.bio.algorithms.alignment.rna.provider.IntraListProvider;
import bgu.bio.algorithms.alignment.rna.provider.PairProvider;
import bgu.bio.algorithms.alignment.rna.provider.SelfProvider;
import bgu.bio.algorithms.alignment.rna.server.AllAgaintAllServer;

/**
 * A set of JUNIT tests for the {@link PairProvider} interface and all its 
 * @author milon
 *
 */
public class TestProviders {
	private List<List<RNASpecificTree>> lists;
	private ArrayList<RNASpecificTree> trees;
	@Before
	public void build(){
		trees = new ArrayList<RNASpecificTree>();
		lists = AllAgaintAllServer.loadTrees(new String[]{"config/tests/GI Intron.fasta","config/tests/tmRNA.fasta"}, new FilterBySizeAndDegree(1, 1), trees);
	}
	
	@After
	public void clean(){
		trees = null;
		lists = null;
	}
	
	@Test(timeout=3000)
	public void testAllAgainstAllProvider() {
		PairProvider provider = new AllAgainstAllProvider();
		int expected = 32640;
		testProvider(provider, expected);
	}
	
	@Test(timeout=3000)
	public void testInnerListProvider() {
		PairProvider provider = new InnerListProvider();
		int expected = 16260;
		testProvider(provider, expected);
	}
	
	@Test(timeout=3000)
	public void testIntraListProvider() {
		PairProvider provider = new IntraListProvider();
		int expected = 16380;
		testProvider(provider, expected);
	}
	
	@Test(timeout=3000)
	public void testSelfProvider() {
		PairProvider provider = new SelfProvider();
		int expected = 256;
		testProvider(provider, expected);
	}
	
	@Test(timeout=3000)
	public void testEvalProvider() {
		PairProvider provider = new EvalProvider(12146);
		int expected = 12146;
		testProvider(provider, expected);
	}
	
	private void testProvider(PairProvider provider,int expected){
		provider.initiate(lists, true);
		Assert.assertEquals("Wrong number of pairs",expected,provider.initialAmount());
		int counter = 0;
		while (provider.hasNext()){
			counter++;			
			IntPair pair = provider.next();
			Assert.assertNotNull("Got null as answer",pair);
			Assert.assertFalse("First index out of list scope", pair.getFirst() < 0 || pair.getFirst() >= trees.size());
			Assert.assertFalse("Second index out of list scope", pair.getSecond() < 0 || pair.getSecond() >= trees.size());
		}
		Assert.assertEquals("Got the wrong amount of items from the provider",expected,counter);
	}

}
