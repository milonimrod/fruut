# README #
This project contains the source code for the publication of [Unrooted unordered homeomorphic subtree alignment of RNA trees](http://dx.doi.org/10.1186/1748-7188-8-13) by Nimrod Milo, Shay Zakov, Erez Katzenelson, Eitan Bachmat, Yefim Dinitz and Michal Ziv-Ukelson. A online version of the tool is available [here](http://www.cs.bgu.ac.il/~negevcb/FRUUT). 

## About ##
RNA secondary structures are often represented as trees, motivating the application of tree comparison tools to the detection of structural similarities and common motifs. We generalize some current approaches for RNA tree alignment, which are traditionally confined to ordered rooted mappings, to also consider unordered unrooted mappings.

### License ###
The code is provided under the [MIT License](http://opensource.org/licenses/MIT) without any warranty.

### Technical information ###
The code is written in Java and thus is available for all OS version (although it was only tested on Windows and Linux). In the Download section you can download a stand alone version of the code. In order to download the source code it is also necessary to download the source code of [this](https://bitbucket.org/milonimrod/bioinformatics-base-tools) project which FRUUT relies on.